# Setup SSO for SOGo

## Set up Go development environment

This should NOT be done on the production machine.

* download Go from https://golang.org/dl/ (choose newest version, as of 06.08.2019 that is 1.12.7)
* unpack the Go archive somewhere (e.g. `/tmp/go`)
* set environment variable `GOROOT` to that location
* add `$GOROOT/bin` to your `PATH`

## Download vouch-proxy

Get vouch at https://github.com/vouch/vouch-proxy/releases.

Version 0.5.8 worked well, 0.6.4 did not.

## Compile vouch-proxy

* unpack vouch archive somewhere (e.g. `/tmp/vouch`)
* change to that folder
* call `./do.sh goget` (this downloads all prerequisites, it cat take some minutes)
* call `./do.sh build`
* after that is done you can delete the Go development environment again

## Install vouch-proxy

* Choose an application target folder (e.g. `/opt/vouch`) and create that folder
* from `/tmp/vouch` copy the resulting binary (in this case `vouch-proxy-0.5.8`) and the subdirectories `static` and `templates` into that target folder

## Configure vouch-proxy

* copy the prepared vouch.yml into the vouch target folder (e.g. `/opt/vouch`)
* edit the file:
    * in jwt/secret: fill in 44 random bytes, e.g. with `pwgen 44`
    * in oauth/client_secret: fill in the secret string from Oauth client Symfony table

## Create systemd unit for vouch-proxy

* copy the prepared `vouch-proxy.service` to `/etc/systemd/system`
* double check especially the binary name (version number!) in ExecStart
* reload the systemd configuration with `systemctl daemon-reload`
* make vouch start automatically when booting: `systemctl enable vouch-proxy`

## Start vouch

`service vouch-proxy start`

If all went well you will see log output in `/var/log/syslog` and vouch should listen on localhost port 9090.

## Configure nginx

* copy the prepared `nginx_config_sso` to `/etc/nginx/sites-available`
* create a symlink into `/etc/nginx/sites-enabled`
* test for syntax errors with `nginx -t`
* `service nginx reload`
